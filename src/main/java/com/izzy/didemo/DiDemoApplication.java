package com.izzy.didemo;

import com.izzy.didemo.controller.ConstructorBasedController;
import com.izzy.didemo.controller.GetterBasedController;
import com.izzy.didemo.controller.MyController;
import com.izzy.didemo.controller.PropertyBasedController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:beanconfig.xml")
public class DiDemoApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(DiDemoApplication.class, args);
        MyController controller = (MyController) context.getBean("myController");
        controller.hello();

        System.out.println(context.getBean(PropertyBasedController.class).sayHello());
        System.out.println(context.getBean(GetterBasedController.class).sayHello());
        System.out.println(context.getBean(ConstructorBasedController.class).sayHello());
        System.out.println(context.getBean(Forecast.class).weather());
        System.out.println(context.getBean(FakeDataSource.class).getPassword());
        System.out.println(context.getBean(FakeDataSource.class).getUrl());
        System.out.println(context.getBean(FakeJMS.class).getUrl());

    }
}
