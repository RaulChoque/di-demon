package com.izzy.didemo.controller;

import org.springframework.stereotype.Controller;

@Controller

public class MyController {
    public String hello() {
        String greet = "Hello Spring";
        System.out.println(greet);
        return greet;
    }
}
