package com.izzy.didemo.controller;

import com.izzy.didemo.service.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller

public class PropertyBasedController {

    //@Qualifier("greetingServiceImpl")
    @Autowired
    public GreetingService greetingService;

    public String sayHello() {
        return greetingService.sayGreeting();
    }
}
