package com.izzy.didemo.service;

import org.springframework.stereotype.Service;

@Service

public class GetterGreetingServiceImpl implements GreetingService {
    @Override
    public String sayGreeting() {
        return "Hello GetterGreetingServiceImpl";
    }
}
