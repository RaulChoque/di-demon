package com.izzy.didemo.service;

public interface GreetingService {
    String sayGreeting();
}
