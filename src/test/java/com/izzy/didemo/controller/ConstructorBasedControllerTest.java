package com.izzy.didemo.controller;

import com.izzy.didemo.Forecast;
import com.izzy.didemo.service.GreetingService;
import com.izzy.didemo.service.GreetingServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ConstructorBasedControllerTest {

    private ConstructorBasedController constructorBasedController;
    private Forecast forecast;

    @Before
    public void before() throws Exception {
        System.out.println("@before");
        GreetingService greetingService = new GreetingServiceImpl();
        forecast = new Forecast();
        constructorBasedController = new ConstructorBasedController(greetingService, forecast);
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("@after");
    }

    @Test
    public void sayHello() {
        System.out.println("@test sayHello");
        String greeting = constructorBasedController.sayHello();
        greeting.length();
    }
}