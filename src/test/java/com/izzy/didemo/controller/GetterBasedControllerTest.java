package com.izzy.didemo.controller;

import com.izzy.didemo.service.GreetingServiceImpl;
import org.junit.Before;
import org.junit.Test;

public class GetterBasedControllerTest {

    private GetterBasedController getterBasedController;

    @Before
    public void before() throws Exception {
        getterBasedController = new GetterBasedController();
        getterBasedController.setGreetingService(new GreetingServiceImpl());
    }

    @Test
    public void sayHello() {
        String greeting = getterBasedController.sayHello();
        greeting.length();
    }
}