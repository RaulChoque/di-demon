package com.izzy.didemo.controller;

import com.izzy.didemo.service.GreetingServiceImpl;
import org.junit.Before;
import org.junit.Test;

public class PropertyBasedControllerTest {

    private PropertyBasedController propertyBasedController;

    @Before
    public void before() {
        propertyBasedController = new PropertyBasedController();
        propertyBasedController.greetingService = new GreetingServiceImpl();

    }

    @Test
    public void sayHello() {
        String greeting = propertyBasedController.sayHello();
        greeting.length();
    }
}